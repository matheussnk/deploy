$(document).ready(function() {
    $("body").addClass("js");
    var $menu = $("#menu"),
      $menulink = $(".menu-link"),
      $menuTrigger = $(".has-subnav > a");
  
    $menulink.click(function(e) {
      e.preventDefault();
      $menulink.toggleClass("active");
      $menu.toggleClass("active");
    });
  
    var toggleLinks = function() {
      if ($(".menu-link").is(":visible")) {
        if ($(".toggle-link").length > 0) {
        } else {
          $(".has-subnav > a").before('<span class="toggle-link"></span>');
          $(".toggle-link").click(function(e) {
            var $this = $(this);
            $this
              .toggleClass("active")
              .siblings("ul")
              .toggleClass("active");
          });
        }
      } else {
        $(".toggle-link").empty();
      }
    };
  
    toggleLinks();
    $(window).bind("resize", toggleLinks);
  });
  